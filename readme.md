# FTTManager 

CRUD API for FTTManager, is a simple api that allows you to manage tasks. 


## Installing

First step is to.

`Clone repo`

Then from root run

`php artisan migrate`

You can then also run the db seeds for test data

`php artisan db:seed`

## Available Actions
The following calls are available


####Retreieving the auth key
This is a very basic example of how to get the auth token, this can be either a `post` or `get` request

`/auth/?email=[email]&password=[password]`


###Making the calls
The following actions needs to be called with the `token` param acquired in the previous step.

Returns all tasks `tasks`

Return specific task `task/{id}`

Deletes a task `task/delete/{id}`

Creates a new task `task/store/`

Updates a task if task id is present `task/store/`

Returns a list of current users `users`

