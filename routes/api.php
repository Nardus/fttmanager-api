<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/auth/', 'AuthenticateController@authenticate');

// get list of tasks
Route::group(['middleware' => ['cors','jwt.auth']], function () {
    Route::get('tasks', 'TaskController@index');
// get specific task
    Route::get('task/{id}', 'TaskController@show');
// delete a task
    Route::get('task/delete/{id}', 'TaskController@destroy');
// update existing task
    Route::any('task/store/', 'TaskController@store');
// create new task
    Route::post('task', 'TaskController@store');
// get list of users
    Route::get('users', 'TaskController@users');
});

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');


