<?php

namespace App\Http\Controllers;

use App\User;
use Faker\Generator;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthenticateController extends Controller {
	/**
	 * @param Request $request
	 * @param Generator $faker
	 * @param JWTAuth $auth
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function authenticate( Request $request, Generator $faker, JWTAuth $auth ) {
		$credentials = $request->only( 'email', 'password' );
		
		try {
			if ( ! $token = $auth::attempt( $credentials ) ) {
				
				if ( ! empty( $credentials['email'] ) && ! empty( $credentials['password'] ) ) {
					if ( User::where( 'email', '=', $credentials['email'] )->exists() ) {
						return response()->json( [ 'error' => 'Email already exist on system' ], 401 );
					} else {
						$user                  = new User();
						$user->name            = $faker->firstName;
						$user->surname         = $faker->lastName;
						$user->email           = $credentials['email'];
						$user->password        = bcrypt( $credentials['password'] );
						$user->date_last_login = null;
						$user->remember_token  = str_random( 10 );
						$user->date_last_login = date( 'Y-m-d H:i:s' );
						
						if ( $user->save() ) {
							$token = $auth::attempt( $credentials );
							
							return response()->json( compact( 'token' ) );
						} else {
							return response()->json( [ 'error' => 'Could not create user' ], 401 );
						}
					}
				} else {
					return response()->json( [ 'error' => 'invalid_credentials' ], 401 );
				}
			}
		} catch ( JWTException $e ) {
			return response()->json( [ 'error' => 'could_not_create_token' ], 500 );
		}
		
		return response()->json( compact( 'token' ) );
	}
}