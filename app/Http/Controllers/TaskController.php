<?php

namespace App\Http\Controllers;

use App\Transformer\UserTransformer;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use EllipseSynergie\ApiResponse\Contracts\Response;
use App\Task;
use App\Transformer\TaskTransformer;

class TaskController extends Controller {
	/**
	 * @var Response
	 */
	protected $response;
	
	/**
	 * TaskController constructor.
	 *
	 * @param Response $response
	 */
	public function __construct( Response $response ) {
		$this->response = $response;
	}
	
	/**
	 * @return mixed
	 */
	public function index() {
		$tasks = Task::all();
		
		// Return a collection of $task with pagination
		return $this->response->withCollection( $tasks, new TaskTransformer() );
	}
	
	/**
	 * @param $id
	 *
	 * @return mixed
	 */
	public function show( $id ) {
		$task = Task::find( $id );
		if ( ! $task ) {
			return $this->response->errorNotFound( 'Task Not Found' );
		}
		
		return $this->response->withItem( $task, new  TaskTransformer() );
	}
	
	/**
	 * @param $id
	 *
	 * @return mixed
	 */
	public function destroy( $id ) {
		$task = Task::find( $id );
		if ( ! $task ) {
			return $this->response->errorNotFound( 'Task Not Found' );
		}
		
		if ( $task->delete() ) {
			return $this->response->withItem( $task, new  TaskTransformer() );
		} else {
			return $this->response->errorInternalError( 'Could not delete a task' );
		}
		
	}
	
	/**
	 * @param Request $request
	 *
	 * @return mixed
	 */
	public function store( Request $request ) {
		if ( ! empty( $request->input( 'id' ) ) ) {
			$task = Task::find( $request->input( 'id' ) );
			if ( ! $task ) {
				return $this->response->errorNotFound( 'Task Not Found' );
			}
			$task->id = $request->input( 'id' );
		} else {
			$task = new Task;
		}
		
		$task->name        = $request->input( 'task' );
		$task->description = $request->input( 'task_description' );
		$task->user_id     = $request->input( 'user_id' );
		$task->due_date    = $request->input( 'due_date' );
		$task->status      = $request->input( 'status' );
		
		if ( $task->save() ) {
			return $this->response->withItem( $task, new  TaskTransformer() );
		} else {
			return $this->response->errorInternalError( 'Could not updated/created a task' );
		}
	}
	
	public function users() {
		$users = User::all();
		
		return $this->response->withCollection( $users, new UserTransformer() );
	}
	
}