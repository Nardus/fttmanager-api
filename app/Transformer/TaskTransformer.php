<?php

namespace app\Transformer;


use App\User;

class TaskTransformer {
	/**
	 * @param $task
	 *
	 * @return array
	 */
	public function transform( $task ) {
		$user = User::find( $task->user_id );
		
		return [
			'id'               => $task->id,
			'task'             => $task->name,
			'task_description' => $task->description,
			'user_id'          => $task->user_id,
			'user_name'        => $user->name . ' ' . $user->surname,
			'due_date'         => $task->due_date,
			'status'           => $task->status
		];
	}
}